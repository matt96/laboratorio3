<?php 
/**
* 
*/
class UsuarioController
{
	
	function __construct()
	{
		
	}

	function index(){
		require_once('Views/Servicio/bienvenido.php');
	}

	function register(){
		require_once('Views/Servicio/register.php');
	}

	function save(){
		if (!isset($_POST['estado'])) {
			$estado="of";
		}else{
			$estado="on";
		}
		$servicio= new Servicio(null, $_POST['nombres'],$_POST['descripcion'],$estado, $_POST['imagen'], $_POST['colaboradores'], $_POST['contacto'], $_POST['precio'], $_POST['categoria']);

		Servicio::save($servicio);
		$this->show();
	}

	function show(){
		$listaServicios=Servicio::all();

		require_once('Views/Servicio/show.php');
	}

	function updateshow(){
		$id=$_GET['idServicio'];
		$servicio=Servicio::searchById($id);
		require_once('Views/Servicio/updateshow.php');
	}

	function update(){
		$servicio = new Servicio($_POST['id'],$_POST['nombres'],$_POST['descripcion'],$_POST['estado'], $_POST['imagen'], $_POST['colaboradores'], $_POST['contacto'], $_POST['precio'], $_POST['categoria']);
		Servicio::update($servicio);
		$this->show();
	}
	function delete(){
		$id=$_GET['id'];
		Servicio::delete($id);
		$this->show();
	}

	function search(){
		if (!empty($_POST['id'])) {
			$id=$_POST['id'];
			$servicio=Servicio::searchById($id);
			$listaServicios[]=$servicio;
			//var_dump($id);
			//die();
			require_once('Views/Servicio/show.php');
		} else {
			$listaServicios=Servicio::all();

			require_once('Views/Servicio/show.php');
		}
		
		
	}

	function error(){
		require_once('Views/Servicio/error.php');
	}

}

?>