<?php 
/**
* 
*/
class Servicio
{
	private $id;
	private $nombres;
	private $descripcion;
	private $estado;
	private $imagen;
	private $colaboradores;
	private $contacto;
	private $precio;
	private $categoria;
	

	
	function __construct($id, $nombres,$descripcion, $estado, $imagen, $colaboradores, $contacto, $precio, $categoria)
	{
		$this->setId($id);
		$this->setNombres($nombres);
		$this->setDescripcion($descripcion);
		$this->setEstado($estado);	
		$this->setImagen($imagen);
		$this->setColaboradores($colaboradores);
		$this->setContacto($contacto);
		$this->setPrecio($precio);
		$this->setCategoria($categoria);	
	}

	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getNombres(){
		return $this->nombres;
	}

	public function setNombres($nombres){
		$this->nombres = $nombres;
	}

	public function getDescripcion(){
		return $this->descripcion;
	}

	public function setDescripcion($descripcion){
		$this->descripcion = $descripcion;
	}

	public function getEstado(){

		return $this->estado;
	}

	public function setImagen($imagen){
		$this->imagen = $imagen;
	}

	public function getImagen(){
		return $this->imagen;
	}

	public function setColaboradores($colaboradores){
		$this->colaboradores = $colaboradores;
	}

	public function getColaboradores(){
		return $this->colaboradores;
	}

	public function setContacto($contacto){
		$this->contacto = $contacto;
	}

	public function getContacto(){
		return $this->contacto;
	}	

	public function setPrecio($precio){
		$this->precio = $precio;
	}

	public function getPrecio(){
		return $this->precio;
	}

	public function setCategoria($categoria){
		$this->categoria = $categoria;
	}

	public function getCategoria(){
		return $this->categoria;
	}	

	public function setEstado($estado){
		
		if (strcmp($estado, 'on')==0) {
			$this->estado=1;
		} elseif(strcmp($estado, '1')==0) {
			$this->estado='checked';
		}elseif (strcmp($estado, '0')==0) {
			$this->estado='of';
		}else {
			$this->estado=0;
		}

	}

	public static function save($servicio){
		$db=Db::getConnect();

		$insert=$db->prepare('INSERT INTO servicio VALUES (NULL, :nombres,:descripcion,:estado, :imagen, :colaboradores, :contacto, :precio, :categoria)');

		$insert->bindValue('nombres',$servicio->getNombres());
		$insert->bindValue('descripcion',$servicio->getDescripcion());
		$insert->bindValue('estado',$servicio->getEstado());
		$insert->bindValue('imagen',$servicio->getImagen());
		$insert->bindValue('colaboradores',$servicio->getColaboradores());
		$insert->bindValue('contacto',$servicio->getContacto());
		$insert->bindValue('precio',$servicio->getPrecio());
		$insert->bindValue('categoria',$servicio->getCategoria());
		$insert->execute();
	}

	public static function all(){
		$db=Db::getConnect();
		$listaServicios=[];

		$select=$db->query('SELECT * FROM servicio order by id');

		foreach($select->fetchAll() as $servicio){
			$listaServicios[]=new Servicio($servicio['id'],$servicio['nombres'],$servicio['descripcion'],$servicio['estado'], $servicio['imagen'], $servicio['colaboradores'], $servicio['contacto'], $servicio['precio'], $servicio['categoria']);
		}
		return $listaServicios;
	}

	public static function searchById($id){
		$db=Db::getConnect();
		$select=$db->prepare('SELECT * FROM servicio WHERE id=:id');
		$select->bindValue('id',$id);
		$select->execute();

		$servicioDb=$select->fetch();


		$servicio = new Servicio ($servicioDb['id'],$servicioDb['nombres'], $servicioDb['descripcion'], $servicioDb['estado'], $servicio['imagen'], $servicio['colaboradores'], $servicio['contacto'], $servicio['precio'], $servicio['categoria']);
		//var_dump($servicio);
		//die();
		return $servicio;

	}

	public static function update($servicio){
		$db=Db::getConnect();
		$update=$db->prepare('UPDATE servicio SET nombres=:nombres, descripcion=:descripcion, estado=:estado, imagen=:imagen, colaboradores=:colaboradores, contacto=:contacto, precio=:precio, categoria=:categoria  WHERE id=:id');

		$update->bindValue('nombres',$servicio->getNombres());
		$update->bindValue('descripcion',$servicio->getDescripcion());
		$update->bindValue('estado',$servicio->getEstado());
		$update->bindValue('imagen',$servicio->getImagen());
		$update->bindValue('colaboradores',$servicio->getColaboradores());
		$update->bindValue('contacto',$servicio->getContacto());
		$update->bindValue('precio',$servicio->getPrecio());
		$update->bindValue('categoria',$servicio->getCategoria());

		$update->bindValue('id',$servicio->getId());
		$update->execute();
	}

	public static function delete($id){
		$db=Db::getConnect();
		$delete=$db->prepare('DELETE  FROM servicio WHERE id=:id');
		$delete->bindValue('id',$id);
		$delete->execute();		
	}
}

?>