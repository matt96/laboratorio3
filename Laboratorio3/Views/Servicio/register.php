<div class="container">
  <h2>Registro de Servicio</h2>
  <form action="?controller=servicio&&action=save" method="POST">
    <div class="row">

      <div class="col-md-6">
        <div class="form-group">
          <label for="text">Nombre:</label>
          <input type="text" class="form-control" id="nombres" placeholder="Ingrese su Nombre" name="nombres">
        </div>   

        <div class="form-group">
          <label for="text">Descripción</label>
          <input type="text" name="descripcion" class="form-control" placeholder="Ingrese una descripcion">
        </div>

        <div class="form-group">
          <label for="text">Imagen</label>
          <input type="text" name="imagen" class="form-control" placeholder="Ingrese url de la imagen">
        </div>

        <div class="form-group">
          <label for="text">Colaboradores</label>
          <input type="text" name="colaboradores" class="form-control" placeholder="numero de colaboradores">
        </div>
        
      </div>   

      <div class="col-md-6">


        <div class="form-group">
          <label for="text">Contacto</label>
          <input type="text" name="contacto" class="form-control" placeholder="telefonos y correos">
        </div>

        <div class="form-group">
          <label for="text">Precio</label>
          <input type="text" name="precio" class="form-control" placeholder="precio">
        </div>

        <div class="form-group">
          <label for="text">Categoria</label>
          <input type="text" name="categoria" class="form-control" placeholder="nombre de la categoria">
        </div>

        <div class="check-box">
          <label>Activo <input type="checkbox" name="estado">  </label>      
        </div>        
      </div>    
    </div>



    <button type="submit" class="btn btn-primary">Guardar</button>
  </form>
</div>