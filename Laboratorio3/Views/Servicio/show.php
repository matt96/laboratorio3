

<div class="container">
	<h2>Lista Servicios</h2>
	<form class="form-inline" action="?controller=servicio&action=search" method="post">
		<div class="form-group row">
			<div class="col-xs-4">
				<input class="form-control" id="id" name="id" type="text" placeholder="Busqueda por ID">
			</div>
		</div>
		<div class="form-group row">
			<div class="col-xs-4">
				<button type="submit" class="btn btn-primary" ><span class="glyphicon glyphicon-search"> </span> Buscar</button>
			</div>
		</div>
	</form>
	<div class="table-responsive">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>Id</th>
					<th>Nombres</th>
					<th>Descipcion</th>
					<th>Colaboradores</th>
					<th>Contacto</th>
					<th>Precio</th>
					<th>Categoria</th>
					<th>Accion</th>					
				</tr>
				<tbody>
					<?php foreach ($listaServicios as $servicio) {?>
					<tr>
						<td> <a href="?controller=servicio&&action=updateshow&&idServicio=<?php  echo $servicio->getId()?>"> <?php echo $servicio->getId(); ?></a> </td>
						<td><?php echo $servicio->getNombres(); ?></td>
						<td><?php echo $servicio->getDescripcion(); ?></td>
						<td><?php echo $servicio->getColaboradores(); ?></td>
						<td><?php echo $servicio->getContacto(); ?></td>
						<td><?php echo $servicio->getPrecio(); ?></td>
						<td><?php echo $servicio->getCategoria(); ?></td>
						<td><?php if ( $servicio->getEstado()=='checked'):?>
							Activo
						<?php  else:?>
							Inactivo
						<?php endif; ?></td>
						<td><a href="?controller=servicio&&action=delete&&id=<?php echo $servicio->getId() ?>">Eliminar</a> </td>
					</tr>
					<?php } ?>
				</tbody>

			</thead>
		</table>

	</div>	

</div>