<div class="container">
	<h2>Actualizar Servicio</h2>
	<form action="?controller=servicio&&action=update" method="POST">

		<input type="hidden" name="id" value="<?php echo $servicio->getId() ?>" >
	    <div class="row">

	      <div class="col-md-6">
	        <div class="form-group">
	          <label for="text">Nombre:</label>
	          <input type="text" class="form-control" id="nombres" name="nombres" value="<?php echo $servicio->getNombres() ?>">
	        </div>   

	        <div class="form-group">
	          <label for="text">Descripción</label>
	          <input type="text" name="descripcion" class="form-control" value="<?php echo $servicio->getDescripcion() ?>">
	        </div>

	        <div class="form-group">
	          <label for="text">Imagen</label>
	          <input type="text" name="imagen" class="form-control" value="<?php echo $servicio->getImagen() ?>">
	        </div>

	        <div class="form-group">
	          <label for="text">Colaboradores</label>
	          <input type="text" name="colaboradores" class="form-control" placeholder="numero de colaboradores" value="<?php echo $servicio->getColaboradores() ?>">
	        </div>
	        
	      </div>   

	      <div class="col-md-6">


	        <div class="form-group">
	          <label for="text">Contacto</label>
	          <input type="text" name="contacto" class="form-control" placeholder="telefonos y correos" value="<?php echo $servicio->getContacto() ?>">
	        </div>

	        <div class="form-group">
	          <label for="text">Precio</label>
	          <input type="text" name="precio" class="form-control" placeholder="precio" value="<?php echo $servicio->getPrecio() ?>">
	        </div>

	        <div class="form-group">
	          <label for="text">Categoria</label>
	          <input type="text" name="categoria" class="form-control" placeholder="nombre de la categoria" value="<?php echo $servicio->getCategoria() ?>">
	        </div>

			<div class="check-box">
				<label>Activo <input type="checkbox" name="estado" <?php echo $servicio->getEstado() ?>></label>
			</div>       
	      </div>    
	    </div>

		<button type="submit" class="btn btn-primary">Actualizar</button>

	</form>
</div>