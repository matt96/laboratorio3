<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Lab 3</title>
	    <meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <!--bootstrap-->
	    <link type="text/css" rel="stylesheet" href="./assets/css/bootstrap.min.css">
	</head>

	<body>
		<header>
			<?php 
				require_once('cabecera.php');
			 ?>
			
		</header>

		<section>
			<?php require_once('routing.php'); ?>
		</section>


	        <!-- bootstrap -->
	        <script src="https://code.jquery.com/jquery-3.3.1.min.js"  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	        <script type="text/javascript" src="./assets/js/bootstrap.min.js"></script>

	</body>

</html>